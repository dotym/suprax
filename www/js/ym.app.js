/**
* ym.app.js v1.4.4 by @alimvedats and @erhanerdgn
* Copyright 2013 (c) YazılıMecra
*/

var app = {
	// Application Constructor
	initialize: function() {
		var t = this;
		this.bindEvents();
		$(window).hashchange();
		FastClick.attach(document.body);
		//this.bcode = $.keyframe.browserCode();
		$(function(){
			t.initCharts();
		});
	},
	chartOptions : {
		base : {
			chart: {
				type: 'column',
				options3d: {
					enabled: true,
					alpha: 13,
					beta: 20,
					viewDistance: 30,
					depth: 60
				},
				marginTop: 68,
				marginRight: 60,
				marginBottom: 50,
				label: {
					items: [{
						html: "<h1>Suprax</h1>"
					}]
				}
			},
			legend: {
				enabled: false,
				itemDistance: 10,
				y: -10
			},
			credits: {
				enabled: false
			},
			yAxis: {
				allowDecimals: false,
				min: 0,
				max: 100,
				tickInterval: 10,
				title: {
					text: ''
				}
			},
			tooltip: {
				shared: true,
				useHTML: true,
				headerFormat: '<table width="100%"><tr><td style="text-align: center"><strong>{point.key}</strong></td></tr>',
				pointFormat: '<tr><td style="text-align: center;font-size:25px;font-weight:bold;"><b>%{point.y}</b></td></tr>',
				footerFormat: '</table>',
				valueDecimals: 0
			},
			plotOptions: {
				column: {
					stacking: 'percent',
					depth: 60,
				}
			}
		},
		//--------------------------------- ÜSYE 1 -----------------------------------------------------------------
		eUSYEa : {
			zoom:1,
			title: {
				text: 'Klinik Etkinlik Oranı (%)'
			},
			xAxis: {
				categories: ["Sefiksim", "Amoksisilin-klavulanat"],
				labels: {
					color: '#fff',
					//x: 5,
					useHTML: true,
					formatter: function() {
						console.log(this);
						return {
							'Sefiksim': '<img src="media/images/main/logo.png" style="width:100px">',
							'Amoksisilin-klavulanat': '<strong>Amoksisilin-klavulanat</strong>'
						}[this.value];
					}
				}
			},
			series: [{
				stacking: 'normal',
				borderWidth: 1,
				borderRadius: 3,
				pointWidth: 90,
				pointPadding: 0,
				shadow: true,
				data: [
					{
						name: 'Sefiksim',
						useHTML: true,
						y: 93,
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(197, 44, 30)'],
								[.50, 'rgb(197, 44, 32)'],
								[.50, 'rgb(197, 44, 36)'],
								[1, 'rgb(197, 44, 38)']
							]
						}
					}, 
					{
						name: 'Amoksisilin-klavulanat',
						y: 90,
						useHTML: true,
						style: {
							padding: '100px 100px',
							marginTop: '100px',
							color: 'white'
						},
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(255, 194, 30)'],
								[.50, 'rgb(255, 194, 20)'],
								[.50, 'rgb(255, 194, 10)'],
								[1, 'rgb(255, 194, 0)']
							]
						}
					}
				]
			}]
		},
		
		
		//--------------------------------- ÜSYE 2 -----------------------------------------------------------------
		eUSYEb : {
			title: {
				text: 'Tedaviyi Bırakma Oranı (%)'
			},
			xAxis: {
				categories: ["Sefiksim", "Amoksisilin-klavulanat"],
				labels: {
					color: '#fff',
					useHTML: true,
					formatter: function() {
						console.log(this);
						return {
							'Sefiksim': '<img src="media/images/main/logo.png" style="width:100px">',
							'Amoksisilin-klavulanat': '<strong>Amoksisilin-klavulanat</strong>'
						}[this.value];
					}
				}
			},
			yAxis: {
				allowDecimals: true,
				min: 0,
				max: 6,
				tickInterval: .5,
				title: {
					text: ''
				}
			},
			series: [{
				stacking: 'normal',
				borderWidth: 1,
				borderRadius: 3,
				pointWidth: 90,
				pointPadding: 0,
				shadow: true,
				data: [
					{
						name: 'Sefiksim',
						useHTML: true,
						y: 0.65,
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(197, 44, 30)'],
								[.50, 'rgb(197, 44, 32)'],
								[.50, 'rgb(197, 44, 36)'],
								[1, 'rgb(197, 44, 38)']
							]
						}
					}, 
					{
						name: 'Amoksisilin-klavulanat',
						y: 5.5,
						useHTML: true,
						style: {
							padding: '100px 100px',
							marginTop: '100px',
							color: 'white'
						},
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(255, 194, 30)'],
								[.50, 'rgb(255, 194, 20)'],
								[.50, 'rgb(255, 194, 10)'],
								[1, 'rgb(255, 194, 0)']
							]
						}
					}
				]
			}]
		},
		//--------------------------------- ASYE -----------------------------------------------------------------
		eASYE : {
			title: {
				text: 'Klinik Etkinlik Oranı (%)'
			},
			xAxis: {
				categories: ["Sefiksim", "Sefuroksim aksetil"],
				labels: {
					color: '#fff',
					useHTML: true,
					formatter: function() {
						console.log(this);
						return {
							'Sefiksim': '<img src="media/images/main/logo.png" style="width:100px">',
							'Sefuroksim aksetil': '<strong>Sefuroksim aksetil</strong>'
						}[this.value];
					}
				}
			},
			yAxis: {
				allowDecimals: false,
				min: 0,
				max: 100,
				tickInterval: 10,
				title: {
					text: ''
				}
			},
			series: [{
				stacking: 'normal',
				borderWidth: 1,
				borderRadius: 3,
				pointWidth: 90,
				pointPadding: 0,
				shadow: true,
				data: [
					{
						name: 'Suprax: 400mg/gün <br>(günde tek doz)',
						useHTML: true,
						y: 91,
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(197, 44, 30)'],
								[.50, 'rgb(197, 44, 32)'],
								[.50, 'rgb(197, 44, 36)'],
								[1, 'rgb(197, 44, 38)']
							]
						}
					}, 
					{
						name: 'Sefuroksim aksetil: 250mg <br>12 saatte bir (günde iki doz)',
						y: 88,
						useHTML: true,
						style: {
							padding: '100px 100px',
							marginTop: '100px',
							color: 'white'
						},
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(255, 194, 30)'],
								[.50, 'rgb(255, 194, 20)'],
								[.50, 'rgb(255, 194, 10)'],
								[1, 'rgb(255, 194, 0)']
							]
						}
					}
				]
			}]
		},
		//--------------------------------- eIYE -----------------------------------------------------------------
		eIYE : {
			title: {
				text: 'Bakteriyel Eradikasyon (%)'
			},
			xAxis: {
				categories: ["Sefiksim", "Siprofloksasin"],
				labels: {
					color: '#fff',
					useHTML: true,
					formatter: function() {
						console.log(this);
						return {
							'Sefiksim': '<img src="media/images/main/logo.png" style="width:100px">',
							'Siprofloksasin': '<strong>Siprofloksasin</strong>'
						}[this.value];
					}
				}
			},
			yAxis: {
				allowDecimals: false,
				min: 0,
				max: 100,
				tickInterval: 10,
				title: {
					text: ''
				}
			},
			series: [{
				stacking: 'normal',
				borderWidth: 1,
				borderRadius: 3,
				pointWidth: 90,
				pointPadding: 0,
				shadow: true,
				data: [
					{
						name: 'Suprax: 49 Hasta 400mg/gün <br>(günde tek doz)',
						useHTML: true,
						y: 95.6,
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(197, 44, 30)'],
								[.50, 'rgb(197, 44, 32)'],
								[.50, 'rgb(197, 44, 36)'],
								[1, 'rgb(197, 44, 38)']
							]
						}
					}, 
					{
						name: 'Siprofloksasin: 55 Hasta 500mg/gün <br>(günde iki doz)',
						y: 66,
						useHTML: true,
						style: {
							padding: '100px 100px',
							marginTop: '100px',
							color: 'white'
						},
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(255, 194, 30)'],
								[.50, 'rgb(255, 194, 20)'],
								[.50, 'rgb(255, 194, 10)'],
								[1, 'rgb(255, 194, 0)']
							]
						}
					}
				]
			}]
		},
		//--------------------------------- cUSYE -----------------------------------------------------------------
		cUSYE : {
			title: {
				text: 'Klinik Etkinlik Oranları (%)'
			},
			xAxis: {
				categories: ["Sefiksim", "Sefaklor"],
				labels: {
					color: '#fff',
					useHTML: true,
					formatter: function() {
						console.log(this);
						return {
							'Sefiksim': '<img src="media/images/main/logo.png" style="width:100px">',
							'Sefaklor': '<strong>Sefaklor</strong>'
						}[this.value];
					}
				}
			},
			yAxis: {
				allowDecimals: false,
				min: 0,
				max: 100,
				tickInterval: 10,
				title: {
					text: ''
				}
			},
			series: [{
				stacking: 'normal',
				borderWidth: 1,
				borderRadius: 3,
				pointWidth: 90,
				pointPadding: 0,
				shadow: true,
				data: [
					{
						name: 'n=63<br>Suprax: 8mg/kg/gün<br>(günde tek doz)',
						useHTML: true,
						y: 97,
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(197, 44, 30)'],
								[.50, 'rgb(197, 44, 32)'],
								[.50, 'rgb(197, 44, 36)'],
								[1, 'rgb(197, 44, 38)']
							]
						}
					}, 
					{
						name: 'Hasta yaşı: 6ay-12yaş<br>Sefaklor: 40mg/kg/gün<br>(günde üç eşit dozda)',
						y: 78,
						useHTML: true,
						style: {
							padding: '100px 100px',
							marginTop: '100px',
							color: 'white'
						},
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(255, 194, 30)'],
								[.50, 'rgb(255, 194, 20)'],
								[.50, 'rgb(255, 194, 10)'],
								[1, 'rgb(255, 194, 0)']
							]
						}
					}
				]
			}]
		},
		//--------------------------------- cASYE -----------------------------------------------------------------
		cASYE : {
			title: {
				text: 'Klinik Etkinlik Oranları (%)'
			},
			xAxis: {
				categories: ["Sefiksim", "Klaritromisin"],
				labels: {
					color: '#fff',
					useHTML: true,
					formatter: function() {
						console.log(this);
						return {
							'Sefiksim': '<img src="media/images/main/logo.png" style="width:100px">',
							'Klaritromisin': '<strong>Klaritromisin</strong>'
						}[this.value];
					}
				}
			},
			yAxis: {
				allowDecimals: false,
				min: 0,
				max: 100,
				tickInterval: 10,
				title: {
					text: ''
				}
			},
			series: [{
				stacking: 'normal',
				borderWidth: 1,
				borderRadius: 3,
				pointWidth: 90,
				pointPadding: 0,
				shadow: true,
				data: [
					{
						name: 'n=110<br>Suprax: 400mg/gün<br>(günde tek doz)',
						useHTML: true,
						y: 88,
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(197, 44, 30)'],
								[.50, 'rgb(197, 44, 32)'],
								[.50, 'rgb(197, 44, 36)'],
								[1, 'rgb(197, 44, 38)']
							]
						}
					}, 
					{
						name: 'n=103<br>Klaritromisin: 250mg 12 saatte bir<br>(günde iki doz)',
						y: 86,
						useHTML: true,
						style: {
							padding: '100px 100px',
							marginTop: '100px',
							color: 'white'
						},
						color: {
							linearGradient: {x1: 0,	y1: 0,x2: 1,y2: 0},
							stops: [
								[0, 'rgb(255, 194, 30)'],
								[.50, 'rgb(255, 194, 20)'],
								[.50, 'rgb(255, 194, 10)'],
								[1, 'rgb(255, 194, 0)']
							]
						}
					}
				]
			}]
		}
	},
	initCharts: function(){
		
		//--------------------------------- cIYEa -----------------------------------------------------------------
		var cIYEa = {
		    chart: {
				type: 'spline'
			},
			title: {
				text: 'Klinik Etkinlik Oranları (%)'
			},
			xAxis: {
				categories: ["0","0-2", "2-4","4-6","6-8","8-12","12-24"],

			},
			yAxis: {
				
				allowDecimals: false,
				min: 0,
				max: 180,
				tickInterval: 20,
				title: {
					text: ''
				}
			},
			plotOptions: {
			                spline: {
			                    marker: {
			                        radius: 4,
			                        lineColor: '#C53222',
			                        lineWidth: 1
			                    }
			                }
			            },
			series: [{
				stacking: 'normal',
				borderWidth: 1,
				borderRadius: 3,
				pointWidth: 90,
				pointPadding: 0,
				shadow: true,
				data: [0, 45, 125, 165, 135, 75,15]
			}]
		};
		

	},
	bindEvents: function() {
		var that = this;
		
		//document.addEventListener('deviceready', that.onDeviceReady, false);

		$(window).hashchange(function(){
						
			var firstSegment = $.url().fsegment(1),
				delayTime = 0;
						
			
			if(that.selectedPage){

				
				
				//log(that.selectedPage.get(0));
				
				var endAnim = that.selectedPage.data('endanimation');
				
				if(endAnim){
					
					that.endAnimation(endAnim);
					delayTime = 0; //default delay time
				}
				
				if(that.subSelectedPageId){
				// çıkılan sayfadki animasyonu kapatıyoruz
					$("#"+that.subSelectedPageId).find("*").stop();
					that.endAnimation(that.subSelectedPageId+'OnUnload');
					
				}
			}
			
			setTimeout( function(){
				if(firstSegment == "history.back"){
					window.history.back();
					return;
				}else{
					firstSegment = firstSegment == "" ? "cover" : firstSegment;
					$('a.activeLink').removeClass('activeLink');
					that.getPage(firstSegment);
				
					
					var activeLink = that.makeActiveLink('a[href="'+window.location.hash+'"]');
				}
			}, delayTime);
			
			return true;
		});
		
	},
	makeActiveLink : function(selector){
		var that = this,
			activeLink = $(selector).not('[data-hideparent="true"]');
			
		if(activeLink.hasClass('activeLink'))
			return;
		
		activeLink.addClass('xxx');
		
		activeLink.addClass('activeLink').each(function(){
			
			var parentLink = $(this).data('parentlink');
			
			if(parentLink){
				that.makeActiveLink('a[href="'+parentLink+'"]');
			}
		});
		
		return true;
	},
	changeCover : function(direction){
		var current = $('.activeCover').animate({ opacity : 0 }, 500, function(){
				$(this).removeClass("activeCover");
			}),
			nextCover = direction == "up" ? current.prev() : current.next();
			
			if(!nextCover.length){
				nextCover = direction == "up" ? current.siblings(':last') : current.siblings(':first');
			}
			
			nextCover.animate({opacity:1},1500, function(){
				$(this).addClass("activeCover");
			});
	},
	selectedPage : null,
	subSegment : null,
	subSelectedPage : null,
	subSelectedPageData : null,
	subSelectedPageId : null,
	getPage : function(pageName){
		
		
		var that = this;
		
			that.selectedPage = $('[data-page]').hide().filter('[data-page="'+ pageName +'"]').show(),
			that.subSegment = $.url().fsegment(2) || "main",
			that.subSelectedPage = null,
			that.subSelectedPageData = null;
			
			var found = that.selectedPage.find('.animatedElement').stop(true,true).attr('style','').removeClass('animatedElement');
		
		if(pageName == "cover"){
			$('.ref').hide();
			$('#subpageItems').not('[data-alwaysvisible]').hide();
			that.startAnimation(that.selectedPage.data('startanimation'));
		}else{
			$('#subpageItems').show();
			that.subSegment = that.subSegment != "" ? that.subSegment : "main";
			
			that.subSelectedPage = that.selectedPage.find('[data-subpage]').hide().filter('[data-subpage="'+ that.subSegment +'"]');
			
			if(that.subSelectedPage.length){
				that.subSelectedPageId = that.subSelectedPage.get(0).id;
				that.subSelectedPageData = that.subSelectedPage.data();
								
				
				$('.onlyOneHidden').show().removeClass('onlyOneHidden');
				
				if(that.subSelectedPageData.hiddens != undefined){
					var elements = that.subSelectedPageData.hiddens.split(',');
					$(elements).each(function(i, el){
						$(el).hide().addClass('onlyOneHidden');
					});
				}
				
				that.subSelectedPage.show();
				
				if(that.subSelectedPageData.activelink != undefined){
					
					that.makeActiveLink('a[href="'+that.subSelectedPageData.activelink+'"]');
				}
				
				if(that.subSelectedPageData.pckslink != undefined){
					$('.pcks').show().attr('href', that.subSelectedPageData.pckslink);				
				}else{
					$('.pcks').hide();
				}
									
				if(that.subSelectedPageData.ref != undefined){
					
					var refButton = $('.ref').show().off('click');
					
					
					if(that.subSelectedPageData.ref.indexOf('select from') >= 0 ){
							
						refButton.on('click', function(){
							
							var ref = subSelectedPage.find(that.subSelectedPageData.ref.replace('select from','')).filter(':visible').data('ref')
							log(ref);
						
							$(ref).modal();
							return false;
						});
						
					}else{
						
						refButton.attr('href', that.subSelectedPageData.ref).on('click', function(){
							$(that.subSelectedPageData.ref).modal();
							return false;
						});	
						
					}
				}else{
					$('.ref').hide();
				}
				
				
				if(that.subSelectedPageData.backlink == undefined){
					$('.back').hide();
				}else if(that.subSelectedPageData.backlink == "history.back"){
					log("hoppala");
					$('.back').show().one('click', function(){
						window.history.back();
						return false;
					});
				}else{
					$('.back').show().attr('href', that.subSelectedPageData.backlink);
				}
				
				if(that.subSelectedPageData.startanimation != undefined){
					that.startAnimation(that.subSelectedPageData.startanimation);
				}
				

				if(that.subSelectedPageId){
					that.startAnimation(that.subSelectedPageId+'OnLoad');
				}

			}
		}
	},
	//23.Oct @alim
	s5 : function() {
		return 'dotym' + (((1+Math.random())*0x10000)|0).toString(16);
	},
	getOrCreateId : function(el){
		var e = $(el), id=e.attr('id'), nid = this.s5();
		if(!id){
			e.attr('id', id=nid);
		}
		return id;
	},
	definedStartAnimations : [],
	defineAnimations : function(name, startCss, endCss){
		var that = this;
		if(!that.definedStartAnimations[name]){
			$(function(){
				$.keyframe.define([{
					'name': name,
					"0%": startCss,
					"100%": endCss
				}]);
			});
			that.definedStartAnimations[name] = true;
		}else{
			
		}
	},
	playKeyframeActions : function(el, startCss, endCss, duration, timingFunction, delay, repeat, cb, direction,fillMode){
		if(!el.hasClass('animatedElement')){
			log('playing');
			var name = this.getOrCreateId(el);
			
			//--- custom addon ----
			
			if(el.data('isselected')){
				name += "dotym";	
			}
			
			//---------------------------
			
			this.defineAnimations(name,startCss, endCss);
			
			$(el).resetKeyframe(function() {
				el.playKeyframe({
					name: name,
					duration: duration || 1000,
					timingFunction: timingFunction ? timingFunction : 'linear',
					delay: delay || 0,
					repeat: repeat || 1, //'infinite',
					direction: direction || 'normal',
					fillMode: fillMode || 'forwards',
					complete: function(){ log('played:'+name); }
				});//.addClass('animatedElement');
			});
		}
	},
	startAnimationList : {
		"graph":function(){
			$('[data-subpage]:visible .graphline').each(function(i,el){
				app.playKeyframeActions($(el), 'width:'+$(el).css("width"), 'width:'+ $(el).data("widthvalue"), 2000, 'ease-in-out', i*2000);
			});
		},
		"opacity":function(){
			$('[data-subpage]:visible .opacityline').each(function(i,el){
				app.playKeyframeActions($(el), 'opacity:0px', 'opacity:'+ $(el).data("opacityvalue"), 2000, 'linear', i*500);
			});
		}
	},
	startAnimation : function(aniName){
		log('startAnimation: ' + aniName);
		if(aniName && this.startAnimationList[aniName]){
			
			return this.startAnimationList[aniName].call(this);
		}else return false;
	},
	endAnimationList : {
		
		/*sayfalardan hook edilebilir...*/
	},
	endAnimation: function(aniName){
		log('endAnimation: ' + aniName);
		var aniID = aniName.replace("OnUnload","");
		log(aniID);
		
		if(this.endAnimationList[aniName]){

			this.endAnimationList[aniName].call();
			
		}
	},
	onDeviceReady: function() {
		//none	
	},
	scrollElement : function(el, distance, duration){
		log('dist:' + distance);
		el.css("-webkit-transition-duration", (duration/1000).toFixed(1) + "s");
		var value = (distance >= 0 ? "" : "-") + Math.abs(distance).toString();
		el.css("-webkit-transform", "translate3d("+value +"px,0px,0px)");
	},
	//----------- 23.Oct 2:39 @alim
	clickAndSwipe : function(jqEl, startPos, endPos){
		var that = this;
		jqEl.on('click', function(){
			var e = $(this),
				isEnd = e.data('isend');
			if(!isEnd){
				that.scrollElement($(this), endPos, 750);
				e.data('isend',true);
			}else{
				that.scrollElement($(this), startPos, 750);
				e.data('isend',false);
			}
		}).swipe({
			 swipeLeft:function(){
				e.stopPropagation();
				that.scrollElement($(this), startPos, 750);
			 },
			 swipeRight:function(e){
				e.stopPropagation();
				that.scrollElement($(this), endPos, 750);
			 },
			 allowPageScroll:"horizontal"
		});
		
	}
};

function log(l){
	console.log(l);
}


/*
 * jQuery hashchange event - v1.3 - 7/21/2010
 * http://benalman.com/projects/jquery-hashchange-plugin/
 * 
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 */
(function($,e,b){var c="hashchange",h=document,f,g=$.event.special,i=h.documentMode,d="on"+c in e&&(i===b||i>7);function a(j){j=j||location.href;return"#"+j.replace(/^[^#]*#?(.*)$/,"$1")}$.fn[c]=function(j){return j?this.bind(c,j):this.trigger(c)};$.fn[c].delay=50;g[c]=$.extend(g[c],{setup:function(){if(d){return false}$(f.start)},teardown:function(){if(d){return false}$(f.stop)}});f=(function(){var j={},p,m=a(),k=function(q){return q},l=k,o=k;j.start=function(){p||n()};j.stop=function(){p&&clearTimeout(p);p=b};function n(){var r=a(),q=o(m);if(r!==m){l(m=r,q);$(e).trigger(c)}else{if(q!==m){location.href=location.href.replace(/#.*/,"")+q}}p=setTimeout(n,$.fn[c].delay)}return j})()})(jQuery,this);

/*
* jQuery doTimeout: Like setTimeout, but better! - v1.0 - 3/3/2010
* http://benalman.com/projects/jquery-dotimeout-plugin/
* 
* Copyright (c) 2010 "Cowboy" Ben Alman
* Dual licensed under the MIT and GPL licenses.
* http://benalman.com/about/license/
*/
(function($){var a={},c="doTimeout",d=Array.prototype.slice;$[c]=function(){return b.apply(window,[0].concat(d.call(arguments)))};$.fn[c]=function(){var f=d.call(arguments),e=b.apply(this,[c+f[0]].concat(f));return typeof f[0]==="number"||typeof f[1]==="number"?this:e};function b(l){var m=this,h,k={},g=l?$.fn:$,n=arguments,i=4,f=n[1],j=n[2],p=n[3];if(typeof f!=="string"){i--;f=l=0;j=n[1];p=n[2]}if(l){h=m.eq(0);h.data(l,k=h.data(l)||{})}else{if(f){k=a[f]||(a[f]={})}}k.id&&clearTimeout(k.id);delete k.id;function e(){if(l){h.removeData(l)}else{if(f){delete a[f]}}}function o(){k.id=setTimeout(function(){k.fn()},j)}if(p){k.fn=function(q){if(typeof p==="string"){p=g[p]}p.apply(m,d.call(n,i))===true&&!q?o():e()};o()}else{if(k.fn){j===undefined?e():k.fn(j===false);return true}else{e()}}}})(jQuery);
