/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
app.initialize();

var lineSpeed = 2; 

app.startAnimationList.coverPageOnLoad = function(){
	$(".secondBg img").animate({width:"100%"},400*lineSpeed);
	$(".coverWarrior img").delay(200*lineSpeed).animate({width:"100%"},300*lineSpeed);

	$(".leftMiddle1").animate({top:265},265*lineSpeed, function(){
		$(".leftBottom1").animate({opacity:1},10*lineSpeed, function(){
			$(".logo.home").animate({top:117},117*lineSpeed, function(){
				$(".square.child .silhouette img").animate({top:0},114*lineSpeed, function(){
					$(".square.child .title").animate({opacity:1},10*lineSpeed, function(){
						$(".square.man .silhouette img").animate({top:0},158*lineSpeed, function(){
							$(".square.man .title").animate({opacity:1},10*lineSpeed);
						});
					});
				});
			});
		});
	});
};

app.endAnimationList.coverPageOnUnload = function(){
	$(".secondBg img, .coverWarrior img").css({width:0});
	$(".leftMiddle1, .logo.home").css({top:-157});
	$(".square.child .silhouette img").css({top:114});
	$(".square.man .silhouette img").css({top:158});
	$(".leftBottom1, .square.child .title, .square.man .title").css({opacity:0});
};

app.startAnimationList.eriskinMainOnLoad = function(){
	$(".eriskinBody img").animate({width:"100%"},300*lineSpeed, function(){
		$(".eriskinBodyTopInfo img").animate({opacity:1},130*lineSpeed);
		$(".eriskinBodyMiddleInfo img").delay(160*lineSpeed).animate({opacity:1},130*lineSpeed);
		$(".eriskinBodyBottomInfo img").delay(420*lineSpeed).animate({opacity:1},130*lineSpeed);
	});
	$(".eriskinMainData").delay(600*lineSpeed).animate({opacity:1},100*lineSpeed);
	$(".leftmenu.eriskin.main").delay(700*lineSpeed).animate({top:0},100*lineSpeed);
	/*
	$(".eriskinBodyTopInfo").delay(200*lineSpeed).animate({opacity:1},10*lineSpeed);
	$(".eriskinBodyMiddleInfo").delay(500*lineSpeed).animate({opacity:1},10*lineSpeed);
	$(".eriskinBodyBottomInfo").delay(800*lineSpeed).animate({opacity:1},10*lineSpeed);
	*/
};
app.endAnimationList.eriskinMainOnUnload = function(){
	$(".eriskinBody img").css({width:0});
	$(".eriskinBodyTopInfo img, .eriskinBodyMiddleInfo img, .eriskinBodyBottomInfo img").css({opacity:0});
	$(".leftmenu.eriskin.main").css({top:-569});
	$(".eriskinMainData").css({opacity:0});
};

app.startAnimationList.cocukMainOnLoad = function(){
	$(".cocukBody img").animate({width:"100%"},300*lineSpeed, function(){
		$(".cocukBodyTopInfo img").animate({opacity:1},130*lineSpeed);
		$(".cocukBodyMiddleInfo img").delay(160*lineSpeed).animate({opacity:1},130*lineSpeed);
		$(".cocukBodyBottomInfo img").delay(420*lineSpeed).animate({opacity:1},130*lineSpeed);
	});
	$(".cocukMainData").delay(600*lineSpeed).animate({opacity:1},100*lineSpeed);
	$(".leftmenu.cocuk.main").delay(700*lineSpeed).animate({top:0},100*lineSpeed);
};
app.endAnimationList.cocukMainOnUnload = function(){
	$(".cocukBody img").css({width:0});
	$(".cocukBodyTopInfo img, .cocukBodyMiddleInfo img, .cocukBodyBottomInfo img").css({opacity:0});
	$(".leftmenu.cocuk.main").css({top:-569});
	$(".cocukMainData").css({opacity:0});
};

app.startAnimationList.dtFormuMainOnLoad = function(){
	$(".bgDtFormu1 img").animate({width:"100%"},178*lineSpeed, function(){
		$(".bgDtFormu2 img").animate({width:"100%"},71*lineSpeed, function(){
			$(".redLine").animate({opacity:1},50*lineSpeed, function(){
				$(".dtSuyla").animate({opacity:1},80*lineSpeed, function(){
					$(".dtSuda").animate({opacity:1},80*lineSpeed, function(){
						$(".dtTablet").animate({opacity:1},80*lineSpeed, function(){
							$(".dtFormuTitle").animate({opacity:1},50*lineSpeed);
						});
					});
				});
			});
		});
	});
};
app.endAnimationList.dtFormuMainOnUnload = function(){
	$(".bgDtFormu1 img, .bgDtFormu2 img").css({width:0});
	$(".redLine, .dtSuyla, .dtSuda, .dtTablet, .dtFormuTitle").css({opacity:0});
};

app.startAnimationList.kubMainOnLoad = function(){
	$(".kubTitle").animate({opacity:1},100*lineSpeed, function(){
		$(".kubTablet").animate({opacity:1},100*lineSpeed, function(){
			$(".kubData").animate({opacity:1},100*lineSpeed);
		});
	});
};
app.endAnimationList.kubMainOnUnload = function(){
	$(".kubTitle, .kubTablet, .kubData").css({opacity:0});
};

app.startAnimationList.pozolojiMainOnLoad = function(){
	$(".pozolojiTitle").animate({opacity:1},50*lineSpeed, function(){
		$(".eriskinImg").animate({opacity:1},80*lineSpeed, function(){
			$(".cocukImg").animate({opacity:1},80*lineSpeed, function(){
				$(".eriskinText").animate({opacity:1},80*lineSpeed, function(){
					$(".cocukText").animate({opacity:1},80*lineSpeed);
				});
			});
		});
	});
};
app.endAnimationList.pozolojiMainOnUnload = function(){
	$(".pozolojiTitle, .eriskinImg, .eriskinText, .cocukImg, .cocukText").css({opacity:0});
};

app.startAnimationList.hastaUyumuMainOnLoad = function(){
	$(".hastaUyumuTitle").animate({opacity:1},50*lineSpeed, function(){
		$(".gtdImg").animate({opacity:1},80*lineSpeed, function(){
			$(".ctImg").animate({opacity:1},80*lineSpeed, function(){
				$(".gtdText").animate({opacity:1},80*lineSpeed, function(){
					$(".ctText").animate({opacity:1},80*lineSpeed);
				});
			});
		});
	});
};
app.endAnimationList.hastaUyumuMainOnUnload = function(){
	$(".hastaUyumuTitle, .gtdImg, .ctImg, .gtdText, .ctText").css({opacity:0});
};

app.startAnimationList.eriskinUsyeOnLoad = function(){
	$('.eriskinUsyeChart.first').highcharts($.extend({},app.chartOptions.base, app.chartOptions.eUSYEa));
	
	$(".eriskinUsyeData").animate({opacity:1},200*lineSpeed, function(){
		$(this).siblings(".bough").animate({opacity:1},100*lineSpeed);
	});
};
app.endAnimationList.eriskinUsyeOnUnload = function(){
	$(".eriskinUsyeData, .bough").css({opacity:0});
	$('.eriskinUsyeChart.first').empty();
};

app.startAnimationList.eriskinUsye2OnLoad = function(){
	$('.eriskinUsyeChart.second').highcharts($.extend({},app.chartOptions.base, app.chartOptions.eUSYEb));
	$(".eriskinUsyeData").animate({opacity:1},200*lineSpeed, function(){
		$(this).siblings(".bough").animate({opacity:1},100*lineSpeed);
	});
};
app.endAnimationList.eriskinUsye2OnUnload = function(){
	$(".eriskinUsyeData, .bough").css({opacity:0});
	$('.eriskinUsyeChart.second').empty();
};

app.startAnimationList.eriskinAsyeOnLoad = function(){
	$('.eriskinAsyeChart').highcharts($.extend({},app.chartOptions.base, app.chartOptions.eASYE));
	$(".eriskinAsyeData").animate({opacity:1},200*lineSpeed, function(){
		$(this).siblings(".bough").animate({opacity:1},100*lineSpeed);
	});
};
app.endAnimationList.eriskinAsyeOnUnload = function(){
	$(".eriskinAsyeData, .bough").css({opacity:0});
	$('.eriskinAsyeChart').empty();
};

app.startAnimationList.eriskinIyeOnLoad = function(){
	$('.eriskinIyeChart').highcharts($.extend({},app.chartOptions.base, app.chartOptions.eIYE));
	$(".eriskinIyeData").animate({opacity:1},200*lineSpeed, function(){
		$(this).siblings(".bough").animate({opacity:1},100*lineSpeed);
	});
};
app.endAnimationList.eriskinIyeOnUnload = function(){
	$(".eriskinIyeData, .bough").css({opacity:0});
	$('.eriskinIyeChart').empty();
};

app.startAnimationList.cocukUsyeOnLoad = function(){
	$('.cocukUsyeChart').highcharts($.extend({},app.chartOptions.base, app.chartOptions.cUSYE));
	$(".cocukUsyeData").animate({opacity:1},200*lineSpeed, function(){
		$(this).siblings(".bough").animate({opacity:1},100*lineSpeed);
	});
};
app.endAnimationList.cocukUsyeOnUnload = function(){
	$(".cocukUsyeData, .bough").css({opacity:0});
	$('.cocukUsyeChart').empty();
};

app.startAnimationList.cocukAsyeOnLoad = function(){
	$('.cocukAsyeChart').highcharts($.extend({},app.chartOptions.base, app.chartOptions.cASYE));
	$(".cocukAsyeData").animate({opacity:1},200*lineSpeed, function(){
		$(this).siblings(".bough").animate({opacity:1},100*lineSpeed);
	});
};
app.endAnimationList.cocukAsyeOnUnload = function(){
	$(".cocukAsyeData, .bough").css({opacity:0});
	$('.cocukAsyeChart').empty();
};

app.startAnimationList.cocukIyeOnLoad = function(){
//	$('.cocukIyeChart.first').highcharts($.extend({},app.chartOptions.base, app.chartOptions.cASYE));
	$(".cocukIyeData").animate({opacity:1},200*lineSpeed, function(){
		$(this).siblings(".bough").animate({opacity:1},100*lineSpeed);
	});
	$(".cocukIyeChartLineFirst.leftSmall").delay(300).animate({width:462}, 462*lineSpeed);
};
app.endAnimationList.cocukIyeOnUnload = function(){
	$(".cocukIyeData, .bough").css({opacity:0});
	$(".cocukIyeChartLineFirst").css({width:0});
//	$('.cocukAsyeChart.first').empty();
};

app.startAnimationList.cocukIye2OnLoad = function(){
	$(".cocukIyeData").animate({opacity:1},200*lineSpeed, function(){
		$(this).siblings(".bough").animate({opacity:1},100*lineSpeed);
	});
};
app.endAnimationList.cocukIye2OnUnload = function(){
	$(".cocukIyeData, .bough").css({opacity:0});
};

app.startAnimationList.videojiMainOnLoad = function(){
	videojs.options.flash.swf = "media/video-js.swf";
	/*
	videojs("supraxVideoElement").ready(function(){
		var myPlayer = this;
		myPlayer.play();
	});
	*/
};

app.endAnimationList.videojiMainOnUnload = function(){
	videojs("supraxVideoElement").ready(function(){
		this.pause();
		this.currentTime(0);
	});
};





/* modal charts event */
$('.modal').on('hidden.bs.modal', function () {
	$(this).find(".modalChart.dynamic").empty();
	$(".cocukIyeChartLineFirst.onModal").css({width:0});
});
$('.modal').on('show.bs.modal', function () {
	var modalChart = $(this).find(".modalChart");
	
	if(typeof modalChart.data("cname") !== "undefined"){
		modalChart.highcharts($.extend({},app.chartOptions.base, app.chartOptions[modalChart.data("cname")]));
	}
	else {
		$(".cocukIyeChartLineFirst.onModal").animate({width:763}, 763*lineSpeed);
	}
});
$(".logo").attr("onClick",'location.href="#/cover"').css({cursor:"pointer"});

function pt(a){
	return a*1366/1920;
}
function myloops(){
	var i = 0;
	while ( ++i < 1920 ) {
		// This block will be executed 100 times.
//		Math.round(num*Math.pow(10,X)) / Math.pow(10,X)
//		console.log( i +" " +Math.round(num*Math.pow(10,X)) / Math.pow(10,X) +"px");
		console.log( i +" " + Math.round(i*1366/1920)+"px");
	}
}