$(document).ready(function(){
	$("img[usemap=#mustela-cover]").myImgMaps({mapList:["home_klinik","home_bebek","home_avokado","home_urunler"]});
	$("img[usemap=#klinik-invivo]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler","sub_klinik_invitro"]});
	$("img[usemap=#klinik-invitro]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler","sub_klinik_invivo"]});
	
	$("img[usemap=#bebek-bariyer]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler","sub_nem","sub_khucre"]});
	$("img[usemap=#bebek-nem]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler","sub_bariyer","sub_khucre"]});
	$("img[usemap=#bebek-kok-hucre]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler","sub_bariyer","sub_nem"]});
	
	$("img[usemap=#avokado-etken-madde]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler","sub_avokado_ketkinlik"]});
	$("img[usemap=#kanitlanmis-etkinlik-bariyer]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler","sub_avokado_emadde","sub_avokado_ketkinlik_nem","sub_avokado_ketkinlik_khucre"]});
	$("img[usemap=#kanitlanmis-etkinlik-nem]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler","sub_avokado_emadde","sub_avokado_ketkinlik_bariyer","sub_avokado_ketkinlik_khucre"]});
	$("img[usemap=#kanitlanmis-etkinlik-kok-hucre]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler","sub_avokado_emadde","sub_avokado_ketkinlik_bariyer","sub_avokado_ketkinlik_nem"]});
	
	$("img[usemap=#urunler-cover]").myImgMaps({mapList:["home","sub_klinik","sub_bebek","sub_avokado","sub_urunler"]});


	$(".plusButton").bind("click",function(){
		
		$(".plusConatiner").css({visibility:"hidden",opacity:1});
		$(".plusButton").css({visibility:"visible",opacity:1});
		
		var container = $(this).data("relationcontainer");
		if(container !="undefined"){
		//	$(this).css("visibility","hidden");
		//	$("#"+container).css("visibility","visible");
			$(this).animate({opacity: 0}, 1000, function(){
				$(this).css("visibility","hidden");
			});
			$("#"+container).css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0}, 1400);
		}
	});
	
	$(".closeButton").bind("click",function(){
		var container = $(this).closest(".plusConatiner");
		$(container).animate({opacity: 0}, 1000, function(){
			$(container).css("visibility","hidden");
		});
		$('.plusButton[data-relationcontainer="'+$(container).attr("id")+'"]').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0}, 1000);
		
	//	$('.plusButton[data-relationcontainer="'+$(container).attr("id")+'"]').css("visibility","visible");
	//	$(container).css("visibility","hidden");
		
	});

});

(function($) {
	$.fn.myImgMaps = function(options) {
		var mapName = $(this).attr("usemap").substring(1);
		var mapList = options.mapList;

		var areaContainer = {};
		areaContainer["home"]			= {href:"#/cover/main", shape:"poly", coords:"53,68,53,85,34,85,33,67,30,66,43,53,44,53,44,54,57,66,57,67,56,67"};
		// anasayfa linkleri
		areaContainer["home_klinik"]			= {href:"#/klinik-calismalar/invivo", shape:"poly", coords:"0,403,193,403,199,469,0,469"};
		areaContainer["home_bebek"]				= {href:"#/bebek-cildi/bariyer", shape:"poly", coords:"0,474,199,474,202,538,0,538"};
		areaContainer["home_avokado"]			= {href:"#/avokado/etken-madde", shape:"poly", coords:"0,545,201,545,201,609,138,610,0,610"};
		areaContainer["home_urunler"]			= {href:"#/urunler/main", shape:"poly", coords:"0,615,201,615,196,680,0,680"};
		
		//subpage menu link
		areaContainer["sub_klinik"]				= {href:"#/klinik-calismalar/invivo", shape:"poly", coords:"0,150,50,150,65,187,75,239,74,272,73,290,60,339,50,361,0,361"};
			areaContainer["sub_klinik_invivo"]	= {href:"#/klinik-calismalar/invivo", shape:"poly", coords:"110,107,136,109,145,120,146,157,144,239,136,247,102,247,93,236,92,199,94,117,102,109"};
			areaContainer["sub_klinik_invitro"]	= {href:"#/klinik-calismalar/invitro", shape:"poly", coords:"110,256,136,258,145,269,146,306,144,388,136,396,102,396,93,385,92,348,94,266,102,258"};
		areaContainer["sub_bebek"]				= {href:"#/bebek-cildi/bariyer", shape:"poly", coords:"0,367,50,367,65,404,75,456,74,489,73,507,60,556,50,578,0,578"};
			areaContainer["sub_bariyer"]		= {href:"#/bebek-cildi/bariyer", shape:"poly", coords:"110,261,136,263,145,274,146,311,144,392,136,400,102,400,93,389,92,352,94,271,102,263"};
			areaContainer["sub_nem"]			= {href:"#/bebek-cildi/nem", shape:"poly", coords:"110,410,136,412,145,423,146,460,144,541,136,549,102,549,93,538,92,501,94,420,102,412"};
			areaContainer["sub_khucre"]			= {href:"#/bebek-cildi/kok-hucre", shape:"poly", coords:"110,559,136,561,145,572,146,609,144,690,136,698,102,698,93,687,92,650,94,569,102,561"};
			
		areaContainer["sub_avokado"]			= {href:"#/avokado/etken-madde", shape:"poly", coords:"0,583,50,583,65,620,75,672,74,705,73,723,60,772,50,794,0,794"};
			areaContainer["sub_avokado_emadde"]			= {href:"#/avokado/etken-madde", shape:"poly", coords:"110,553,136,555,145,566,146,603,144,685,136,693,102,693,93,682,92,645,94,563,102,555"};
			areaContainer["sub_avokado_ketkinlik"]			= {href:"#/avokado/kanitlanmis-etkinlik-bariyer", shape:"poly", coords:"110,702,136,704,145,715,146,752,144,834,136,842,102,842,93,831,92,794,94,712,102,704"};
				areaContainer["sub_avokado_ketkinlik_bariyer"]			= {href:"#/avokado/kanitlanmis-etkinlik-bariyer", shape:"poly", coords:"273,192,382,194,390,202,390,236,379,245,343,246,265,244,257,236,257,202,265,194"};
				areaContainer["sub_avokado_ketkinlik_nem"]			= {href:"#/avokado/kanitlanmis-etkinlik-nem", shape:"poly", coords:"414,192,523,194,531,202,531,236,520,245,484,246,406,244,398,236,398,202,406,194"};
				areaContainer["sub_avokado_ketkinlik_khucre"]			= {href:"#/avokado/kanitlanmis-etkinlik-kok-hucre", shape:"poly", coords:"555,192,664,194,672,202,672,236,661,245,625,246,547,244,539,236,539,202,547,194"};
		
		areaContainer["sub_urunler"]			= {href:"#/urunler/main", shape:"poly", coords:"0,799,50,799,65,836,75,888,74,921,73,939,60,988,50,1010,0,1010"};

		var mapHTML = "";
						
		mapHTML +='<map name="'+mapName+'">';
		$.each(mapList, function(key,value) {
			var items = areaContainer[value];
			var extra = "";
			if (typeof items.dataToggle != 'undefined') {
				extra = 'data-toggle="modal"';
			}
			mapHTML += '<area shape="'+items.shape+'" coords="'+items.coords+'" href="'+items.href+'" '+extra+'>';
		});
		mapHTML += '</map>';
		$(this).after(mapHTML);
//<area shape="poly" coords="1278,586,1296,599,1295,623,1285,637,1274,641,1267,646,1249,647,1238,636,1238,614,1263,587" href="#patojenler-info-modal" alt="" data-toggle="modal"/>
    }
})(jQuery);